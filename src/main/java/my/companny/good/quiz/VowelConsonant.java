package my.companny.good.quiz;

import java.util.Scanner;

public class VowelConsonant {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a string");
        String str = scanner.nextLine();

        vowel(str);
        System.out.println();
        consonant(str);
    }

    public static void vowel(String kata){
//        String kata = "halo dunia";

        for(int i=0; i<kata.length(); i++) {
            if(kata.split("")[i].equals("a") || kata.split("")[i].equals("i") || kata.split("")[i].equals("u") || kata.split("")[i].equals("e") || kata.split("")[i].equals("o")) {
                System.out.print(kata.split("")[i]+ " ");
            }
        }
    }

    public static void consonant(String kata){
        for(int i=0; i<kata.length(); i++) {
            if(kata.split("")[i].equals("b") || kata.split("")[i].equals("c") || kata.split("")[i].equals("d") || kata.split("")[i].equals("f") || kata.split("")[i].equals("g") || kata.split("")[i].equals("h") || kata.split("")[i].equals("j") || kata.split("")[i].equals("k") || kata.split("")[i].equals("l") || kata.split("")[i].equals("m") || kata.split("")[i].equals("n") || kata.split("")[i].equals("p") || kata.split("")[i].equals("q") || kata.split("")[i].equals("r") || kata.split("")[i].equals("s") || kata.split("")[i].equals("t") || kata.split("")[i].equals("v") || kata.split("")[i].equals("w") || kata.split("")[i].equals("x") || kata.split("")[i].equals("y") || kata.split("")[i].equals("z")) {
                System.out.print(kata.split("")[i]+ " ");
            }
        }
    }

    public static void printlength(String kata){
        System.out.println("Length of the string is: " + kata.length());
    }
}
