package my.companny.good.quiz;

import java.util.Scanner;

public class Divisor {
    static void pembagi(int n)
    {
        for (int i=1;i<=n;i++)
            if (n%i==0)
                System.out.print(i+" ");
    }

    public static void main(String args[])
    {
        Scanner sc=new Scanner(System.in);
        System.out.println("masukkan angka: ");
        int n=sc.nextInt();

        System.out.println("pembagi dari "+n+" adalah:");

        pembagi(n);
    }
}
