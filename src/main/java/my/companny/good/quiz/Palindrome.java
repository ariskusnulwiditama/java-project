package my.companny.good.quiz;

import java.util.Scanner;

public class Palindrome {
    public static String isPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return "palindrome";
        }
        int i = 0;
        int j = s.length() - 1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return "bukan palindrome";
            }
            i++;
            j--;
        }
        return "palindrome";
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("masukkan kata : " );
        String s = scanner.nextLine();
        System.out.println("hasil : " + isPalindrome(s));
    }
}
