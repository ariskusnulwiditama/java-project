package my.companny.good.quiz;

import java.util.Scanner;

public class Counter {
    public static void main(String[] args) {
//    select menu
     mainMenu();
     Scanner scanner = new Scanner(System.in);

     int pilihan = scanner.nextInt();
     while (pilihan != 3){
         switch (pilihan){
             case 1:
                 System.out.println("1. Luas Segitiga");
                 System.out.println("2. Luas Lingkaran");
                 System.out.println("3. Luas Persegi");
                 System.out.println("4. Luas Persegi Panjang");
                 System.out.println("5. Kembali");
                 System.out.print("Pilihan : ");
                 int pilihan1 = scanner.nextInt();
                 switch (pilihan1){
                     case 1:
                         System.out.print("Masukkan Alas : ");
                         int alas = scanner.nextInt();
                         System.out.print("Masukkan Tinggi : ");
                         int tinggi = scanner.nextInt();
                         System.out.println("Luas Segitiga = " + (alas * tinggi) / 2);
                         break;
                     case 2:
                         System.out.print("Masukkan Jari-jari : ");
                         int jari = scanner.nextInt();
                         System.out.println("Luas Lingkaran = " + 3.14 * jari * jari);
                         break;
                     case 3:
                         System.out.print("Masukkan Sisi : ");
                         int sisi = scanner.nextInt();
                         System.out.println("Luas Persegi = " + sisi * sisi);
                         break;
                     case 5:
                         mainMenu();
                         pilihan = scanner.nextInt();
                         break;
                     default:
                         System.out.println("Pilihan tidak ada");
                 }
                 break;
             case 2:
                 System.out.println("1. Volume Balok");
                 System.out.println("2. Volume Bola");
                 System.out.println("3. Volume Kerucut");
                 System.out.println("4. Volume Tabung");
                 System.out.println("5. Kembali");
                 System.out.print("Pilihan : ");
                 int pilihan2 = scanner.nextInt();
                 switch (pilihan2){
                     case 1:
                         System.out.print("Masukkan Panjang : ");
                         int panjang = scanner.nextInt();
                         System.out.print("Masukkan Lebar : ");
                         int lebar = scanner.nextInt();
                         System.out.print("Masukkan Tinggi : ");
                         int tinggi = scanner.nextInt();
                         System.out.println("Volume Balok = " + panjang * lebar * tinggi);
                         break;
                     case 2:
                         System.out.print("Masukkan Jari-jari : ");
                         int jari = scanner.nextInt();
                         System.out.println("Volume Bola = " + 4/3 * 3.14 * jari * jari * jari);
                         break;
                     case 3:
                         System.out.print("Masukkan Jari-jari : ");
                         int jari1 = scanner.nextInt();
                         System.out.print("Masukkan Tinggi : ");
                         int tinggi1 = scanner.nextInt();
                         System.out.println("Volume Kerucut = " + 3.14 * jari1 * jari1 * tinggi1 / 3);
                         break;
                     case 4:
                         System.out.print("Masukkan Jari-jari : ");
                         int jari2 = scanner.nextInt();
                         System.out.print("Masukkan Tinggi : ");
                         int tinggi2 = scanner.nextInt();
                         System.out.println("Volume Tabung = " + 3.14 * jari2 * jari2 * tinggi2);
                         break;
                     case 5:
                         mainMenu();
                         pilihan = scanner.nextInt();
                         break;
                     default:
                         System.out.println("Pilihan tidak ada");

                 }
         }
     }
    }

    private static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void mainMenu() {
        System.out.println("Select menu");
        System.out.println("1. Bangun Datar");
        System.out.println("2. Bangun Ruang");
        System.out.println("3. Keluar");
        System.out.print("Pilihan : ");
    }
}
